const express = require("express");
const path = require("path");
const hbs = require("express-handlebars");
const dotenv = require("dotenv");
const morgan = require("morgan");
const { Client, Config, CheckoutAPI } = require("@adyen/api-library");
const app = express();

// setup request logging
app.use(morgan("dev"));
// Parse JSON bodies
app.use(express.json());

// Parse URL-encoded bodies
app.use(express.urlencoded({ extended: true }));
// Serve client from build folder
app.use(express.static(path.join(__dirname, "/public")));

dotenv.config({
  path: "./.env",
});


// Adyen Node.js API library boilerplate (configuration, etc.)
const config = new Config();
config.apiKey = process.env.API_KEY;
config.merchantAccount = process.env.MERCHANT_ACCOUNT;
const client = new Client({ config });
client.setEnvironment("TEST");
const checkout = new CheckoutAPI(client);

const paymentDataStore = {};

app.engine(
  "handlebars",
  hbs({
    defaultLayout: "main",
    layoutsDir: __dirname + "/views/layouts",
    partialsDir: __dirname + "/views/partials"
  })
);

app.set("view engine", "handlebars");


/* #### Client Side Endpoints ### */

// Render cart page
app.get("/", (req, res) => res.render("preview"));

// Render Checkout page (make a payment)
app.get("/checkout", async (req, res) => {
  try {
    const response = await checkout.paymentMethods({
    	amount: {
    		currency: "USD",
    		value: 1000,
    	},
    	countryCode: "US",
    	channel: "Web",
    	merchantAccount: process.env.MERCHANT_ACCOUNT
    });
    res.render("payment", {
      originKey: process.env.ORIGIN_KEY,
      response: JSON.stringify(response)
    });
  } catch (err) {
    console.error(`Error: ${err.message}, error code: ${err.errorCode}`);
    res.status(err.statusCode).json(err.message);
  }
});


// Authorised result page
app.get("/success", (req, res) => res.render("success"));

// Pending result page
app.get("/pending", (req, res) => res.render("pending"));

// Error result page
app.get("/error", (req, res) => res.render("error"));

// Refused result page
app.get("/failed", (req, res) => res.render("failed"));

/* ### End Client Side Endpoints ### */


/* ### API Endpoints ### */
app.post("/submitPayment", async (req, res) => {
	try{
		//console.log(req.body.data.paymentMethod);
		const response = await checkout.payments({
			amount: { currency: "USD", value: 1000 },
		    paymentMethod: req.body.data.paymentMethod, // Data object passed from onSubmit event of the front end
		    reference: "ORDER_100",
		    merchantAccount: process.env.MERCHANT_ACCOUNT,
		    returnUrl: "http://localhost:8080"
			});

	let paymentMethodType = req.body.data.paymentMethod.type;
    let resultCode = response.resultCode;
    let redirectUrl = response.redirect !== undefined ? response.redirect.url : null;
    let action = null;

    res.json({ paymentMethodType, resultCode, redirectUrl, action});
    
   } catch (err) {
    console.error(`Error: ${err.message}, error code: ${err.errorCode}`);
    res.status(err.statusCode).json(err.message);
	}
	
});

/* ### End API Endpoints ### */

// Start server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));





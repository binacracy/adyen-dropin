const paymentMethodsResponse = JSON.parse(
  document.getElementById("paymentMethodsResponse").innerHTML
);
const originKey = document.getElementById("originKey").innerHTML;

const configuration = {
 paymentMethodsResponse, // The `/paymentMethods` response from the server.
 originKey,
 locale: "en-US",
 environment: "test",
 showPayButton: true,
 onSubmit: (state, dropin) => {
     // Your function calling your server to make the `/payments` request
     makePayment(state)
       .then(response => {
        console.log(response);
         if (response.action) {
           // Drop-in handles the action object from the /payments response
           dropin.handleAction(response.action);
         } else {
           // Your function to show the final result to the shopper
              showFinalResult(response);
         }
       })
       .catch(error => {
         throw Error(error);
       });
   },
 onAdditionalDetails: (state, dropin) => {
   // Your function calling your server to make a `/payments/details` request
   console.log("Inside onAdditionalDetails");
   makeDetailsCall(state.data)
     .then(response => {
       if (response.action) {
         // Drop-in handles the action object from the /payments response
         dropin.handleAction(response.action);
       } else {
         // Your function to show the final result to the shopper
         showFinalResult(response);
       }
     })
     .catch(error => {
       throw Error(error);
     });
 },
 paymentMethodsConfiguration: {
   card: { // Example optional configuration for Cards
     hasHolderName: true,
     holderNameRequired: true,
     enableStoreDetails: true,
     hideCVC: false, // Change this to true to hide the CVC field for stored cards
     name: "Credit or debit card",
     amount: {
      value: 6500,
      currency: "USD"
     }
   }
 }
};


function makePayment(data){
  return fetch("/submitPayment", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  }).then(res => res.json());
}


function showFinalResult(response){
  
  switch(response.resultCode){
    case "Authorised":
        window.location.href = "/success";
        break;
      case "Pending":
        window.location.href = "/pending";
        break;
      case "Refused":
        window.location.href = "/failed";
        break;
      default:
        window.location.href = "/error";
        break;
  }
}


const checkout = new AdyenCheckout(configuration);

const dropin = checkout.create('dropin').mount("#dropin-container");
